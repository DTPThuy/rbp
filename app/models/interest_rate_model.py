import uuid
import json
import joblib
import constants
from datetime import datetime

constant = constants.read_json_file("constants.json")

# Validate fields cho API 1
def validate_required_fields(input_data):
    missing_keys = []
    invalid_values = []
    incorrect_data_type = []

    for field in ["request_type"]:
        if field not in input_data:
            missing_keys.append(field)

    request_body = input_data.get("request_body", {})

    for field in constant["required_fields"]:
        if field not in request_body:
            missing_keys.append(field)

    # Kiểm tra định dạng request_date (yyyyMMdd HH:mm:SS)
    if "request_date" not in missing_keys and input_data["request_date"] is not None:
        try:
            datetime.strptime(input_data["request_date"], '%Y%m%d %H:%M:%S')
        except ValueError:
            constants.add_incorrect_data_type(incorrect_data_type, "request_date",
                                              str(type(input_data["request_date"])).split(
                                                  "'")[1],
                                              "str with format 'yyyyMMdd HH:mm:SS'")

    # Kiểm tra request_type
    if "request_type" not in missing_keys:
        if not isinstance(input_data["request_type"], str):
            constants.add_incorrect_data_type(incorrect_data_type, "request_type",
                                              str(type(input_data["request_type"])).split(
                                                  "'")[1],
                                              "str")
        elif input_data["request_type"] not in constant['valid_request_types']:
            constants.add_incorrect_data_type(invalid_values, "request_type",
                                              input_data["request_type"],
                                              constant['valid_request_types'])

    # Kiem tra request body
    # Kiểm tra product_type
    if "product_type" not in missing_keys and input_data["request_body"]["product_type"] is not None:
        if not isinstance(input_data["request_body"]["product_type"], str):
            constants.add_incorrect_data_type(incorrect_data_type, "product_type",
                                              str(type(input_data["request_body"]["product_type"])).split(
                                                  "'")[1],
                                              "str")
        elif input_data["request_body"]["product_type"] != "CD":
            constants.add_incorrect_data_type(invalid_values, "product_type",
                                              input_data["request_body"]["product_type"],
                                              "CD")

    # Kiểm tra định dạng submission_date (nếu có)
    if "submission_date" not in missing_keys and input_data["request_body"]["submission_date"] is not None:
        try:
            datetime.strptime(
                input_data["request_body"]["submission_date"], '%Y%m%d %H:%M:%S')
        except ValueError:
            constants.add_incorrect_data_type(incorrect_data_type, "submission_date",
                                              str(type(input_data["request_body"]["submission_date"])).split(
                                                  "'")[1],
                                              "str with format 'yyyyMMdd HH:mm:SS'")

    # Kiểm tra giá trị số
    for key in constant['numeric_keys']:
        if key not in missing_keys:
            value = input_data["request_body"].get(key)
            if value is not None:
                if not isinstance(value, (float)):
                    constants.add_incorrect_data_type(incorrect_data_type, key,
                                                      str(type(value)).split(
                                                          "'")[1],
                                                      "float")
                elif value < 0 or value > 1:
                    constants.add_incorrect_data_type(invalid_values, key,
                                                      value,
                                                      "Between 0 and 1")

    # Kiểm tra additional_absolute_cost là số nguyên dương (nếu có)
    if "additional_absolute_cost" not in missing_keys:
        additional_absolute_cost = input_data["request_body"].get(
            "additional_absolute_cost")
        if additional_absolute_cost is not None:
            if not isinstance(additional_absolute_cost, int):
                constants.add_incorrect_data_type(incorrect_data_type, "additional_absolute_cost", str(
                    type(additional_absolute_cost)).split("'")[1], "int")
            elif additional_absolute_cost < 0:
                constants.add_incorrect_data_type(invalid_values, "additional_absolute_cost",
                                                  additional_absolute_cost,
                                                  "Additional absoluate cost > 0")

    return missing_keys, invalid_values, incorrect_data_type

# Validate values cho API 1


def validate_field_values(input_data):
    invalid_values = []
    incorrect_data_type = []

    # Kiem tra trong application_data
    for field, accepted in constant['accepted_values'].items():
        if field in input_data["request_body"]["application_data"]:
            value = input_data["request_body"]["application_data"][field]
            if str(type(value)).split("'")[1] != "NoneType":
                if not isinstance(value, str) and field not in constant['numeric_bool_application_data']:
                    constants.add_incorrect_data_type(incorrect_data_type, field,
                                                      str(type(value)).split(
                                                          "'")[1],
                                                      "str")
                elif field in constant['numeric_application_data']:
                    min_value, max_value = accepted
                    if not (isinstance(value, int) or isinstance(value, float)):
                        constants.add_incorrect_data_type(incorrect_data_type, field,
                                                          str(type(value)).split(
                                                              "'")[1],
                                                          "int or float")
                    elif not (min_value <= value <= max_value):
                        constants.add_incorrect_data_type(
                            invalid_values, field, value, f"Between {min_value} and {max_value}")
                elif field == "is_insured":
                    if not isinstance(value, bool):
                        constants.add_incorrect_data_type(incorrect_data_type, field,
                                                          str(type(value)).split(
                                                              "'")[1],
                                                          "boolean")
                elif value not in accepted:
                    constants.add_incorrect_data_type(
                        invalid_values, field, value, accepted)

    return invalid_values, incorrect_data_type

# Hàm kiểm tra trường có tồn tại trong request_body và trả về giá trị null cho các trường không tồn tại hoặc False


def set_null_for_fields(input_data, request_body_to_check, app_to_check):
    input_data["request_id"] = str(uuid.uuid4())
    input_data["request_date"] = datetime.now().strftime('%Y%m%d %H:%M:%S')
    input_data["request_body"]['application_id'] = str(uuid.uuid4())

    if "request_body" in input_data:
        request_body = input_data["request_body"]
        for field in request_body_to_check:
            if field not in request_body or not request_body[field]:
                request_body[field] = None
        if isinstance(request_body, list):
            for app_data in request_body:
                if "application_data" in app_data:
                    application_data = app_data["application_data"]
                    for field in app_to_check:
                        if field not in application_data or not application_data[field]:
                            application_data[field] = None
        elif isinstance(request_body, dict) and "application_data" in request_body:
            application_data = request_body["application_data"]
            for field in app_to_check:
                if field not in application_data or not application_data[field]:
                    application_data[field] = None


def set_null_for_fields2(input_data, request_body_to_check, app_to_check):
    input_data["request_id"] = str(uuid.uuid4())
    input_data["request_date"] = datetime.now().strftime('%Y%m%d %H:%M:%S')

    # Kiểm tra request_body có tồn tại và là một danh sách (list) hay không
    if "request_body" in input_data and isinstance(input_data["request_body"], list):
        for item in input_data["request_body"]:
            # Kiểm tra từng trường trong danh sách request_body_to_check
            for field in request_body_to_check:
                # Kiểm tra xem trường field có tồn tại trong item không
                if field not in item:
                    # Nếu không tồn tại, đặt giá trị của trường đó thành None
                    item[field] = None
            if "application_data" in item:
                application_data = item["application_data"]

                # Lặp qua các trường trong app_to_check và đặt giá trị null cho các trường không tồn tại hoặc False
                for field in app_to_check:
                    if field not in application_data or not application_data[field]:
                        application_data[field] = None

# create respone cho API 1


def create_response_api_single(request_id, application_id, missing_keys, incorrect_data_type, invalid_values):
    response_status_code, response_status_description = constants.response_status(
        missing_keys, incorrect_data_type, invalid_values)

    response = {
        "response_id": str(uuid.uuid4()),
        "response_date": datetime.now().strftime('%Y%m%d %H:%M:%S'),
        "request_id": request_id,
        "response_status_code": response_status_code,
        "response_status_description": response_status_description,
        "response_body": {
            "application_id": application_id,
            "missing_key": missing_keys,
            "incorrect_data_type": incorrect_data_type,
            "invalid_value": invalid_values
            # "pd_score": model_pipeline.pd_score,
            # "survival_rate": model_pipeline.survival_rate,
            # "survival_time": model_pipeline.survival_time,
            # "gross_revenue_ratio": model_pipeline.gross_revenue_ratio,
            # "risk_loss": model_pipeline.risk_loss,
            # "interest_rate": model_pipeline.interest_rate,
            # "loan_value_projection": model_pipeline.loan_value_projection
        }
    }

    return json.dumps(response, indent=4)

# API 2


def validate_request(input_data):
    missing_keys = []
    invalid_values = []
    incorrect_data_type = []

    # Kiểm tra request_type
    if "request_type" not in input_data:
        missing_keys.append("request_type")
    elif input_data["request_type"] not in constant['valid_request_types']:
        constants.add_incorrect_data_type(
            invalid_values, "request_type", input_data["request_type"], constant['valid_request_types'])

    # Kiểm tra request_date
    if "request_date" in input_data and input_data["request_date"] is not None:
        try:
            datetime.strptime(input_data["request_date"], '%Y%m%d %H:%M:%S')
        except ValueError:
            constants.add_incorrect_data_type(incorrect_data_type, "request_date",
                                              str(type(input_data["request_date"])).split(
                                                  "'")[1],
                                              "str with format 'yyyyMMdd HH:mm:SS'")

    return missing_keys, invalid_values, incorrect_data_type


def validate_required_fields2(input_data):
    missing_keys = []
    invalid_values = []
    incorrect_data_type = []

    for field in constant['required_fields']:
        if field not in input_data:
            missing_keys.append(field)

    if "product_type" in input_data:
        if input_data["product_type"] != "CD":
            constants.add_incorrect_data_type(
                invalid_values, "product_type", "CD", input_data["product_type"])

    # Kiểm tra định dạng submission_date (nếu có)
    if "submission_date" not in missing_keys and input_data.get("submission_date") is not None:
        try:
            datetime.strptime(input_data["submission_date"], '%Y%m%d %H:%M:%S')
        except ValueError:
            constants.add_incorrect_data_type(incorrect_data_type, "submission_date",
                                              str(type(input_data["submission_date"])).split(
                                                  "'")[1],
                                              "str with format 'yyyyMMdd HH:mm:SS'")

    # Kiểm tra giá trị số
    for key in constant['numeric_keys']:
        if key not in missing_keys:
            value = input_data.get(key)
            if value is not None:
                if not isinstance(value, (float)):
                    constants.add_incorrect_data_type(incorrect_data_type, key,
                                                      str(type(value)).split(
                                                          "'")[1],
                                                      "float")
                elif value < 0 or value > 1:
                    constants.add_incorrect_data_type(
                        invalid_values, key, value, "Between 0 and 1")

    # Kiểm tra additional_absolute_cost là số nguyên dương (nếu có)
    if "additional_absolute_cost" not in missing_keys:
        additional_absolute_cost = input_data.get("additional_absolute_cost")
        if additional_absolute_cost is not None:
            if not isinstance(additional_absolute_cost, int):
                constants.add_incorrect_data_type(incorrect_data_type, "additional_absolute_cost",
                                                  str(type(additional_absolute_cost)).split(
                                                      "'")[1],
                                                  "int")
            elif additional_absolute_cost < 0:
                constants.add_incorrect_data_type(
                    invalid_values, "additional_absolute_cost", additional_absolute_cost, "Additional absolute cost > 0")

    return missing_keys, invalid_values, incorrect_data_type


def validate_field_values2(input_data):
    accepted_values = constant['accepted_values']
    invalid_values = []
    incorrect_data_type = []

    # if "request_body" in input_data and isinstance(input_data["request_body"], list):
    #     for item in input_data["request_body"]:
    if "application_data" in input_data:
        application_data = input_data["application_data"]
        for field, accepted in accepted_values.items():
            if field in application_data:
                value = application_data[field]
                if str(type(value)).split("'")[1] != "NoneType":
                    if field in constant['numeric_application_data']:
                        min_value, max_value = accepted
                        if not isinstance(value, (int, float)):
                            constants.add_incorrect_data_type(incorrect_data_type, field,
                                                              str(type(value)).split(
                                                                  "'")[1],
                                                              "int or float")
                        elif not (min_value <= value <= max_value):
                            constants.add_incorrect_data_type(
                                invalid_values, field, value, f"Between {min_value} and {max_value}")
                    elif field == "is_insured":
                        if not isinstance(value, bool):
                            constants.add_incorrect_data_type(incorrect_data_type, field,
                                                              str(type(value)).split(
                                                                  "'")[1],
                                                              "boolean")
                    else:
                        if value not in accepted:
                            constants.add_incorrect_data_type(
                                invalid_values, field, value, accepted)

    return invalid_values, incorrect_data_type


# Hàm để tải model pipelines từ tệp và đặt chúng thành biến toàn cục
def load_model_pipelines(filename):
    global model_pipeline  # Đặt các biến toàn cục
    model_pipeline = joblib.load(filename)
