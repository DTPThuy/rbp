from datetime import datetime
import json

def response_status(missing_keys, incorrect_data_type, invalid_values):
    response_status_code = 1  # Mã trạng thái mặc định là thành công (success)
    response_status_description = "success"

    if missing_keys:
        response_status_code = 2  # Mã trạng thái báo thiếu key
        response_status_description = "missing_key"

    if incorrect_data_type:
        response_status_code = 3  # Mã trạng thái báo sai data type
        response_status_description = "incorrect_data_type"

    if invalid_values:
        response_status_code = 4  # Mã trạng thái báo giá trị không hợp lệ
        response_status_description = "invalid_value"

    if missing_keys and incorrect_data_type:
        response_status_code = 5  # Mã trạng thái báo thiếu key và sai data type
        response_status_description = "missing_key and incorrect_data_type"

    if incorrect_data_type and invalid_values:
        response_status_code = 6  # Mã trạng thái báo sai data type và giá trị không hợp lệ
        response_status_description = "incorrect_data_type and invalid_value"

    if missing_keys and invalid_values:
        response_status_code = 7  # Mã trạng thái báo thiếu key và giá trị không hợp lệ
        response_status_description = "missing_key and invalid_value"

    if missing_keys and incorrect_data_type and invalid_values:
        # Mã trạng thái báo thiếu key, sai data type, và giá trị không hợp lệ
        response_status_code = 8
        response_status_description = "missing_key and incorrect_data_type and invalid_value"

    return response_status_code, response_status_description


def add_incorrect_data_type(incorrect_data_type, key_name, data_type_received, expected_format):
    incorrect_data_type.append({
        "key_name": key_name,
        "data_type_received": data_type_received,
        "data_type_expected": expected_format
    })


def add_invalid_values(invalid_values, key_name, data_type_received, data_type_expected):
    invalid_values.append({
        "key_name": key_name,
        "data_type_received": data_type_received,
        "data_type_expected": data_type_expected
    })

def read_json_file(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as json_file:
            data = json.load(json_file)
        return data
    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
        return None
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON in file '{file_path}': {e}")
        return None