from flask import Flask, request
import uuid
import constants
from datetime import datetime
from models import interest_rate_model as md

app = Flask(__name__)
constant = constants.read_json_file("constants.json")

# # Đặt đường dẫn đến tệp lưu trữ mô hình
# md.load_model_pipelines(model_filename)


@app.route('/risk-based-pricing/cd/interest_rate_calculation_single', methods=['POST'])
def interest_rate_calculation_single():
    # Lấy dữ liệu từ request
    request_data = request.get_json()
    request_id = request_data.get("request_id", str(uuid.uuid4()))
    application_id = request_data.get("request_body", {}).get(
        "application_id", str(uuid.uuid4()))

    # Kiểm tra các trường trong request_body và application_data và đặt giá trị null cho các trường không tồn tại hoặc False
    md.set_null_for_fields(
        request_data, constant['request_body_to_check'], constant['app_to_check'])

    missing_keys, invalid_values, incorrect_data_type = md.validate_required_fields(
        request_data)
    invalid_values2, incorrect_data_type2 = md.validate_field_values(
        request_data)
    # Nối danh sách invalid_values và invalid_values2
    invalid_values += invalid_values2
    # Nối danh sách incorrect_data_type và incorrect_data_type2
    incorrect_data_type += incorrect_data_type2

    response = md.create_response_api_single(
        request_id, application_id, missing_keys, incorrect_data_type, invalid_values)
    return response


@app.route('/risk-based-pricing/cd/interest_rate_calculation_batch', methods=['POST'])
def interest_rate_calculation_batch():
    # Tạo response body
    response_body = []
    # Lấy dữ liệu từ request
    request_data = request.get_json()
    request_id = request_data.get("request_id", str(uuid.uuid4()))

    request_missing_keys, request_invalid_values, request_incorrect_data_type = md.validate_request(
        request_data)

    for item in request_data["request_body"]:
        application_id = item.get("application_id", str(uuid.uuid4()))
        # # Lấy danh sách các đối tượng "Application" từ "request_body"
        md.set_null_for_fields2(
            request_data, constant['request_body_to_check'], constant['app_to_check'])
        missing_keys, invalid_values, incorrect_data_type = md.validate_required_fields2(
            item)
        invalid_values2, incorrect_data_type2 = md.validate_field_values2(item)

        missing_keys += request_missing_keys
        # Nối danh sách invalid_values và invalid_values2
        invalid_values += invalid_values2
        invalid_values += request_invalid_values
        # Nối danh sách incorrect_data_type và incorrect_data_type2
        incorrect_data_type += incorrect_data_type2
        incorrect_data_type += request_incorrect_data_type

        # Tạo một entry cho mỗi application
        response_entry = {
            "application_id": application_id,
            "missing_key": missing_keys,
            "incorrect_data_type": incorrect_data_type,
            "invalid_value": invalid_values
            # "pd_score": model_pipeline.pd_score,
            # "survival_rate": model_pipeline.survival_rate,
            # "survival_time": model_pipeline.survival_time,
            # "gross_revenue_ratio": model_pipeline.gross_revenue_ratio,
            # "risk_loss": model_pipeline.risk_loss,
            # "interest_rate": model_pipeline.interest_rate,
            # "loan_value_projection": model_pipeline.loan_value_projection
        }

        response_body.append(response_entry)

    response_status_code, response_status_description = constants.response_status(
        missing_keys, incorrect_data_type, invalid_values)

    # Tạo response
    response = {
        "response_id": str(uuid.uuid4()),
        "response_date": datetime.now().strftime('%Y%m%d %H:%M:%S'),
        "request_id": request_id,
        "response_status_code": response_status_code,
        "response_status_description": response_status_description,
        "response_body": response_body
    }

    return response


if __name__ == '__main__':
    app.run(debug=True)
